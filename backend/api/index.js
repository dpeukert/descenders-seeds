const express = require('express');
const jwt = require('jsonwebtoken');
const checkAuthHeader = require('auth-header');
const User = require('./users/schema.js');

const router = express.Router();

// router.use(require('mongo-throttle')({  // TODO: fix throttle
// 	'mongoose': {
// 		'uri': process.env['DESCENDERS_MONGO']
// 	},
// 	'rateLimit': {
// 		'ttl': 1800,
// 		'max': 1000
// 	}
// }));
router.use(express.json());
router.use(express.urlencoded({ 'extended': true }));
router.use(require('express-sanitizer')());
router.use(require('hpp')());

let setUser = (req, next, user) => {
	req.user = user;
	next();
}

router.use((req, res, next) => {
	const authHeader = (req.headers.authorization ? checkAuthHeader.parse(req.headers.authorization) : null);
	if(authHeader && authHeader.scheme === 'Bearer' && authHeader.token !== '') {
		jwt.verify(authHeader.token, process.env['DESCENDERS_JWT'], { 'algorithms': ['HS256'] }, (err, payload) => {
			if(payload && payload.sub && !err) {
				User.findById(payload.sub).then(user => { // user = null or an instance of User
					setUser(req, next, user);
				}).catch(err => {
					console.error(`ERROR | Query for ID ${payload.sub} failed:`, err);
					setUser(req, next, null);
				});
			} else {
				if(err) console.error('ERROR | JWT token has no ID, not logging in, payload', payload);
				setUser(req, next, null);
			}
		});
	} else {
		setUser(req, next, null);
	}
});

for(let item of require('fs').readdirSync(__dirname)) {
	if(item !== 'index.js') router.use(`/${item}`, require(`./${item}/routes.js`));
}

module.exports = router;
