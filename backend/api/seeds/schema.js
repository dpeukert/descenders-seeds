let mongoose = require('mongoose');

let Seed = mongoose.Schema({
	_id: {
		type: Number,
		required: true,
		min: 10000000000,
		max: 42388899999
	},
	name: {
		type: String,
		required: true
	},
	user: {
		type: String,
		required: true,
		ref: 'User'
	},
	uploaded: {
		type: Date,
		required: true,
		default: Date.now
	},
	featured: {
		type: Date,
		required: false
	},
	likedBy : [{
		type: String,
		ref: 'User'
	}]
});

Seed.statics.findWithScore = function(query) {
	return this.aggregate([{
		'$match': query
	}, {
		'$lookup': { 'from': 'users', 'localField': 'user', 'foreignField': '_id', 'as': 'user' }
	}, {
		'$unwind': '$user'
	}, {
		'$addFields' : { 'score': { '$size': '$likedBy' } }
	}]);
};

module.exports = mongoose.model('Seed', Seed);
