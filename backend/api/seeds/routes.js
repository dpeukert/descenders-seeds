const express = require('express');
let router = express.Router();
let Seed = require('./schema');

let getSeed = (seed, user) => {
 	return { // can't use a schema method because of https://stackoverflow.com/a/30042373
 		'seed': seed._id,
 		'name': seed.name,
 		'score': seed.score,
 		'liked': (user && seed.likedBy.indexOf(user._id) != -1 ? true : false),
 		'uploaded': seed.uploaded,
 		'user': {
 			'id': seed.user._id,
 			'name': seed.user.name
 		}
 	};
 };

router.get('/:id', (req, res) => {
    Seed.findWithScore({ '_id': +req.params.id }).then(seeds => {
        if(seeds.length === 0) {
            res.json({'success': false});
        } else {
            res.json({'success': true, 'data': (seeds[0] ? getSeed(seeds[0], req.user) : null)});
        }
    }).catch(err => {
        res.json({'success': false, 'error': 'An error occurred while getting the seed'});
    });
});

router.post('/:id/like', (req, res) => {
    if(req.user) {
        Seed.findById(req.params.id).then(seed => {
            if(seed === null ) {
                res.json({'success': false});
            } else if(seed.likedBy.indexOf(req.user._id) === -1) {
                Seed.updateOne({ '_id': req.params.id }, { '$push': { 'likedBy': req.user._id } }).then(() => {
                    res.json({'success': true, 'data': seed.likedBy.length + 1});
                }).catch(err => {
                    res.json({'success': false, 'error': 'An error occurred while liking'});
                });
            } else {
                res.json({'success': true, 'data': seed.likedBy.length});
            }
        }).catch(err => {
            res.json({'success': false, 'error': 'An error occurred while liking'});
        });
    } else {
        res.json({'success': false});
    }
});

router.post('/:id/unlike', (req, res) => {
    if(req.user) {
        Seed.findById(req.params.id).then(seed => {
            if(seed === null ) {
                res.json({'success': false});
            } else if(seed.likedBy.indexOf(req.user._id) !== -1) {
                Seed.updateOne({ '_id': req.params.id }, { '$pull': { 'likedBy': req.user._id } }).then(() => {
                    res.json({'success': true, 'data': seed.likedBy.length - 1});
                }).catch(err => {
                    res.json({'success': false, 'error': 'An error occurred while unliking'});
                });
            } else {
                res.json({'success': true, 'data': seed.likedBy.length});
            }
        }).catch(err => {
            res.json({'success': false, 'error': 'An error occurred while unliking'});
        });
    } else {
        res.json({'success': false});
    }
});

router.get('/search/:sort/:query?', (req, res) => {
    let sort = req.params.sort;
    let params = [], query = {};

    if(req.params.query) {
        for (let param of req.params.query.split(';')) params.push(param.split('='));
        for (let param of params) query[param[0]] = (param[1] ? param[1] : {'$exists': true});
    }

    Seed.findWithScore(query).sort(sort).then(seeds => {
        let returnSeeds = [];
        for (let seed of seeds) returnSeeds.push(getSeed(seed, req.user));
        res.json({'success': true, 'data': returnSeeds});
    }).catch(err => {
        res.json({'success': false, 'error': 'An error occurred while getting the seeds'});
    });
});

router.post('/submit', (req, res) => {
    if(req.user && req.body.seed && req.body.name) { // TODO: check if seed is correct
        new Seed({
            '_id': req.body.seed,
            'name': req.sanitize(req.body.name).trim(),
            'user': req.user._id
        }).save().then(() => {
            res.json({'success': true});
        }).catch(err => {
            res.json({'success': false, 'error': 'Unable to save the seed'});
        });
    } else {
        res.json({'success': false});
    }
});

module.exports = router;
