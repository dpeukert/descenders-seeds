let mongoose = require('mongoose');

let User = mongoose.Schema({
    _id: {
		type: String,
		required: true
	},
    avatar: {
        type: String,
        required: false
    },
    name: {
        type: String,
        required: false
    },
    registered: {
        type: Date,
        required: true,
        default: Date.now
    },
    admin: {
        type: Boolean,
        default: false,
        required: true
    }
});

User.methods.getUser = function() {
    return {
        'id': this._id,
        'registered': this.registered,
        'avatar': this.avatar,
        'name': this.name
    };
};

module.exports = mongoose.model('User', User);
