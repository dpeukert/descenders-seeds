const router = require('express').Router();
const User = require('./schema');

router.use('/auth', require('./auth.js'));

router.get('/me', (req, res) => {
    res.json({'success': true, 'data': (req.user ? req.user.getUser() : null)});
});

router.get('/:id', (req, res) => {
    User.findById(req.params.id).then(user => {
        res.json({'success': true, 'data': (user ? user.getUser() : null)});
    }).catch(e => {
        res.json({'success': false, 'error': 'An error occurred while getting the user'});
    });
});

module.exports = router;
