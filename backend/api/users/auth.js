const express = require('express');
const qs = require('querystring');
const fetch = require('node-fetch');
const jwt = require('jsonwebtoken');
const router = express.Router();
const User = require('./schema');

let sendLoginResponse = (res, obj) => {
    res.send(`
    <!DOCTYPE html>
    <html lang="en">
    <head>
    	<meta charset="UTF-8">
    	<title>Steam Authentication</title>
    </head>
    <body>
        <script>
            if(window.opener) window.opener.authResponse = '${JSON.stringify(obj)}';
            window.close();
        </script>
    </body>
    </html>
    `);
};

router.get('/', (req, res) => {
    res.redirect('https://steamcommunity.com/openid/login?' + qs.stringify({
        'openid.ns': 'http://specs.openid.net/auth/2.0',
        'openid.mode': 'checkid_setup',
        'openid.identity': 'http://specs.openid.net/auth/2.0/identifier_select',
        'openid.claimed_id': 'http://specs.openid.net/auth/2.0/identifier_select',
        'openid.realm': `${req.protocol}://${req.get('host')}/`,
        'openid.return_to': `${req.protocol}://${req.get('host') + req.baseUrl}/return`
    }));
});

router.get('/return', (req, res) => {
    const q = req.query;
    const params = ['openid.ns', 'openid.mode', 'openid.op_endpoint', 'openid.claimed_id', 'openid.identity', 'openid.return_to', 'openid.response_nonce', 'openid.assoc_handle', 'openid.signed', 'openid.sig'];
    let hasEveryProp = true;

    for (let param of params) {
        if(q[param] === undefined) {
            hasEveryProp = false;
            break;
        }
    }

    if(hasEveryProp && q['openid.return_to'] == `${req.protocol}://${req.get('host') + req.baseUrl}/return`) {
        q['openid.mode'] = 'check_authentication';

        fetch('https://steamcommunity.com/openid/login?' + qs.stringify(q)).then(resp => {
            if(!resp.ok) sendLoginResponse(res, {'success': false, 'error': 'Unable to verify request'});
            else return resp.text();
        }).then(resp => {
            let verificationResp = {};

            for (let line of resp.split('\n')) {
                if(line === '' || line.indexOf(':') === -1) continue;
                let lineParts = line.split(':');
                verificationResp[lineParts[0]] = lineParts[1];
            }

            if(verificationResp['is_valid'] === 'true') {
                const steamid = q['openid.claimed_id'].match(/\d+$/)[0];
                let user = { '_id': steamid };

                fetch(`http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=189AE521D35B3F4D0042B1F9A0A53DEB&steamids=${steamid}`).then(resp2 => {
                    if(resp2.ok) return resp2.json();
                    else sendLoginResponse(res, {'success': false, 'error': 'Unable to connect to the Steam API'});
                }).then(resp2 => {
                    if(resp2.response.players[0]) return resp2.response.players[0];
                    else sendLoginResponse(res, {'success': false, 'error': 'Unable to get a user profile from the Steam API (your profile might be private)'});
                }).then(resp2 => {
                    user['avatar'] = resp2.avatarfull;
                    user['name'] = resp2.personaname;
                    return User.findByIdAndUpdate(steamid, user, { 'new': true, 'upsert': true, 'setDefaultsOnInsert': true });
                }).then(resp2 => {
                    const token = jwt.sign({ 'sub': steamid }, process.env['DESCENDERS_JWT'], { 'algorithm': 'HS256' });
                    sendLoginResponse(res, {'success': true, 'data': { 'user': resp2.getUser(), 'token': token }});
                }).catch(err => {
                    console.error(`ERROR | Update/insert for ID ${steamid} failed:`, err);
                    sendLoginResponse(res, {'success': false, 'error': 'Unable to find/create an account'});
                });
            } else {
                sendLoginResponse(res, {'success': false, 'error': 'Forged request'});
            }
        }).catch(err => {
            sendLoginResponse(res, {'success': false, 'error': 'Unable to verify request'});
        });
    } else {
        sendLoginResponse(res, {'success': false, 'error': 'Invalid request'});
    }
});

module.exports = router;
