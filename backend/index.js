const mongoose = require('mongoose');

mongoose.connect(process.env['DESCENDERS_MONGO'], { 'reconnectTries': Number.MAX_VALUE }).then(() => {
    const express = require('express');
    const app = express();

    app.use(require('helmet').frameguard({ action: 'deny' })) // other headers are handled by nginx-proxy
    // TODO: CSP
    
    app.locals.https = (app.get('env') === 'production');
    if(app.locals.https) app.set('trust proxy', true);

    // Backend
    app.use('/api', require('./api/'));

    // Frontend
    app.use(require('connect-history-api-fallback')());
    app.use(express.static('./public'));

    app.listen(8080);
}).catch(err => {
    console.error('ERROR | Unable to connect to database:', err);
});
