# Descenders Seeds

## Not maintained

## Prerequisites
* mongod
* brunch - `npm install -g brunch`
* `cd backend && npm install`
* `cd frontend && npm install`
* environment variables with a MongoDB URI and a JWT secret.

```bash
DESCENDERS_MONGO="mongodb://localhost/descendersseeds"
DESCENDERS_JWT="YouShouldProbablyChangeThisCorrectHorseBatteryStaple1123456!$"
```

## Development
* `cd backend && mkdir db && mongod --dbpath ./db` - starts a local MongoDB instance
* `cd backend && npm start` - provides the API and serves the frontend files
* `cd frontend && npm start` - watches the frontend directory and rebuilds the frontend if needed

## Deployment
* Docker image

## License
The contents of this repository are provided under the GPLv3 license or any later version [determined by the Creative Commons acting in a role of a proxy to be compatible with CC BY-SA 4.0](http://creativecommons.org/compatiblelicenses) (SPDX identifier: `GPL-3.0-or-later`) with the following exceptions and attributions:
* The image assets from [RageSquid](https://ragesquid.com/), the developers of [Descenders](https://www.descendersgame.com/), used with an attribution in this README
  * `frontend/app/assets/img/logo.svg`
  * `frontend/app/assets/img/{canyon,forest,highlands,peaks}.png`
  * `misc/img-originals/{canyon,forest,highlands,peaks}.orig.jpg`
* The [cross origin popup close event workaround](https://stackoverflow.com/a/48240128) by [Dustin Hoffner](https://stackoverflow.com/users/4102308/dustin-hoffner) [licensed](https://stackoverflow.com/posts/48240128/timeline) under the [CC BY-SA 3.0 license](https://creativecommons.org/licenses/by-sa/3.0/) which [is forward compatible with CC BY-SA 4.0](https://meta.stackoverflow.com/a/322803) which [is compatible with GPLv3](https://wiki.creativecommons.org/wiki/ShareAlike_compatibility:_GPLv3)
  * part of `frontend/app/app.vue`
