FROM node:10.8.0-alpine
ENV NODE_ENV=production
WORKDIR /node
COPY ./backend/package.json /node/package.json
RUN npm install
COPY ./backend/ /node/
CMD npm start
EXPOSE 8080
