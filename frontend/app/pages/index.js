import Vue from 'vue';
import Router from 'vue-router';
Vue.use(Router);

import notFound from './notFound';
import newest from './newest';
import featured from './featured';
import popular from './popular';
import search from './search';
import submit from './submit';
import user from './user';
import seedPage from './seedPage';

export default new Router({
    mode: 'history',
    routes: [
        { 'path': '*', 'component': notFound },
        { 'path': '/', 'title': 'Newest', 'component': newest },
        { 'path': '/featured', 'title': 'Featured', 'component': featured },
        { 'path': '/popular', 'title': 'Popular', 'component': popular },
        { 'path': '/search', 'title': 'Search', 'component': search },
        { 'path': '/submit', 'title': 'Submit', 'component': submit },
        { 'path': '/user/:id', 'component': user, 'props': true },
        { 'path': '/seed/:id', 'component': seedPage, 'props': true }
        // TODO: fix user and seed pages not reloading when going from one user/seed to another
        // TODO: fix likes when logging in/out - fetching the seeds again will probably be enough
    ]
});
