import Vue from 'vue';

import Vuex from 'vuex';
Vue.use(Vuex);

import VueMoment from 'vue-moment';
Vue.use(VueMoment);

import FontAwesome from '@fortawesome/fontawesome';
import VueFontAwesome from '@fortawesome/vue-fontawesome'
import { faThumbsUp as faThumbsUpR, faUser, faClock, faCalendarPlus } from '@fortawesome/fontawesome-free-regular';
import { faThumbsUp as faThumbsUpS } from '@fortawesome/fontawesome-free-solid';
import { faSteam } from '@fortawesome/fontawesome-free-brands'
FontAwesome.library.add(faThumbsUpR, faUser, faClock, faCalendarPlus, faThumbsUpS, faSteam);
Vue.component('fa', VueFontAwesome);

import mixins from './mixins.js';
Vue.mixin(mixins);

import app from './app';
import router from './pages/index.js';

const store = new Vuex.Store({
    'state': {
        'user': null
    },
    'mutations': {
        setUser (state, payload) {
            state.user = payload;
        }
    }
});

new Vue({
    'el': '#app',
    'store': store,
    'router': router,
    'render': h => h(app)
});
