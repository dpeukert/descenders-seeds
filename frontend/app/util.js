export const worlds = {
	'1': 'Highlands',
	'2': 'Forest',
	'3': 'Canyon',
	'4': 'Peaks'
};

export const modifiers = {
	'0': 'None',
	'1': 'Boss jump',
	'2': 'No path'
};

export const styles = {
	'0': 'None',
	'1': 'Night',
	'2': 'Dawn',
	'3': 'Storm'
};
