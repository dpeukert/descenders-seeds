export default {
	'methods': {
		api(url, body) {
			return new Promise((resolve, reject) => {
				let opt = { 'credentials': 'same-origin', 'method': (body ? 'POST' : 'GET'), 'headers': {} };

				if(body) {
					opt.body = JSON.stringify(body);
					opt.headers['Content-Type'] = 'application/json';
				}

				if(localStorage.getItem('token')) {
					opt.headers['Authorization'] = `Bearer ${localStorage.getItem('token')}`;
				}

				fetch(`/api${url}`, opt).then(res => {
					if(!res.ok) throw new Error();
					else return res.json();
				}).then(res => {
					if(!res.success) throw new Error(res.error);
					else resolve(res.data);
				}).catch(err => {
					// TODO: flash
					// https://www.npmjs.com/package/vue-flash
					// https://www.npmjs.com/package/vue-flash-message
					// https://fontawesome.com/icons/times?style=solid
					reject(err);
				});
			});
		}
	},
	'computed': {
		user () {
			return this.$store.state.user;
		}
	}
}
