module.exports = {
    'paths': {
        'public': '../backend/public'
    },
    'files': {
        'javascripts': { 'joinTo': 'app.js' }
    },
    'modules': {
        'autoRequire': { 'app.js': [ 'main' ] }
    },
    'plugins': {
        'vue': {
            'extractCSS': true,
            'out': '../backend/public/app.css'
            // TODO: fix FontAwesome css being inlined
        }
    }
};
